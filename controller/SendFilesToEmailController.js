
const fs = require('fs');
const path = require( "path");

const mailer = require('../config/CommonMailer.js');
const TradeReportRequestModel = require( '../models/TradeReportRequestSchema.js');
const EmailModel = require('../models/EmailSchema.js');


class SendFilesToEmailController {
    
    //function  call logged in userdata 
    // SP details,

    static SendMail = async (request, res) => {
        
        const get_all_email_request = await TradeReportRequestModel.find({ mail_status: 0,file_create_status: 1, send_request_type: 'mail' })
        // console.log("get_all_email_request",get_all_email_request);
        const EmailCredentials = await EmailModel.find();
        if(EmailCredentials != ''){
            var from_email=EmailCredentials[0]['email']
            var password = EmailCredentials[0]['password']
        }else{
            var from_email=''
            var password = ''
        }

        get_all_email_request && get_all_email_request.map((data)=>{
              // const user_details = await UserModel.find({ 'user_name': data.client_code });
              const FilesFolder = data.report_file_path
              const email_to = data.email_to
              const type = data.file_format
              fs.readdir(FilesFolder, (err, files) => {
                files.forEach(file => {
                    
                    let client_code = file.split('_')[0];
                    mailer('mamata.microlan@gmail.com', FilesFolder+file ,type,from_email,password,data._id,FilesFolder,email_to,data.report_send_path,file)
                    
                    
                });
              });
        })
       
       
        
    }
}
module.exports = SendFilesToEmailController

