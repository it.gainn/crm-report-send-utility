const config = {
    port: parseInt(process.env.DB_PORT, 10),
    server: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database:process.env.DB_Database2, //'TradeplusNew',
    stream: false,
    
    options: {
    //   trustedConnection: true,
      encrypt: false,
    //   enableArithAbort: true,
      trustServerCertificate: true,
  
    },
}

export default config;