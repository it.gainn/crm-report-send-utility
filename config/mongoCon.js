import { MongoClient } from 'mongodb'
import dotenv from "dotenv"
dotenv.config()
const DATABASE_URL = process.env.DATABASE_URL
const dbs = process.env.DATABASE

const client = new MongoClient(DATABASE_URL)
let res = await client.connect();
let db = res.db(dbs)


export default db;