const mongoose = require("mongoose");


const connectDB = async(DATABASE_URL) =>{
    try {
        const DB_OPTIONS = {
            dbName: 'gainn-fintech'
        }
        await mongoose.connect(DATABASE_URL,DB_OPTIONS)
        console.log("ok");
    } catch (error) {
        console.log(error)
    }
}

// export default connectDB;
module.exports = connectDB




