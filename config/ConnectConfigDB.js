

function ConnectConfigDB(DB_Database){
    try {
        const config = {
            port: parseInt(process.env.DB_PORT, 10),
            server: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database:DB_Database, 
            stream: false,
            
            options: {
            // trustedConnection: true,
            encrypt: false,
            // enableArithAbort: true,
            trustServerCertificate: true,
        
            }

        }
        return config
    } catch (error) {
        console.log(error)
    }
}

export default ConnectConfigDB;