const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
const cron = require("node-cron");

const dotenv = require("dotenv")
dotenv.config()

const SendFilesToEmailcontroller = require("./controller/SendFilesToEmailcontroller.js")

const routes = require('./routes/routes.js')
const connectDB = require('./config/connectdb.js');
app.use('/', router);
app.use('/', routes)
connectDB('mongodb://0.0.0.0:27017/')
app.get('/',function(req,res){ 
  res.sendFile(path.join(__dirname+'/index.html'));
});
cron.schedule("*/10 * * * * *", function (request) {
   SendFilesToEmailcontroller.SendMail(request)
});


app.listen(process.env.PORT || 5000);


console.log('Web Server is listening at port '+ (process.env.PORT || 5000));