// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    name:{type:String,required:true,trim:true},
    email:{type:String,required:true,trim:true},
    password:{type:String,required:true,trim:true},
    role:{type:String,required:true,trim:true},
    user_name:{type:String,required:true,trim:true},
    
    token:{type:String,trim:true},
    date: { type: Date, default: Date.now },

})


// Create Model
const UserModel = mongoose.model('users',userSchema)

export default UserModel