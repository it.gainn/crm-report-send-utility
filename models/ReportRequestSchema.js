// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

import mongoose from "mongoose"


// Defining Schema
const ReportRequestSchema = new mongoose.Schema({

    client_code: {
        type: String, trim: true
    },
    email_to: {
        type: String, trim: true
    },
    email_from: {
        type: String, trim: true
    },
    file_format: {
        type: String, trim: true
    },
    send_request_type: {
        type: String, trim: true
    },
    number: {
        type: String, trim: true
    },
    filename: {
        type: String, trim: true
    },
    document: {
        type: String, trim: true
    },
    telegram: {
        type: String, trim: true
    },
    caption: {
        type: String, trim: true
    },

    from_date: {
        type: Date,
    },
    to_date: {
        type: Date,
    },
    report_name_id: {
        type: mongoose.Schema.ObjectId, trim: true
    },
    status: {
        type: String, trim: true, default: '0'
    },
    mail_status: {
        type: String, trim: true, default: '0'
    },
    created_by: {
        type: mongoose.Schema.ObjectId, trim: true, required: true
    },

    // dynemic cronjob
    FromDt:{
        type:Date,
       required:true
    },
    ToDt:{
        type:Date,
        required:true
    },
    txt1:{
        type:String,
        default:""
    },
    txt2:{
        type:String,
        default:""
    },
    txt3:{
        type:String,
        default:""
    },
    Fil1Num:{
        type:Number,
        default:0
    },
    Fil2Num:{
        type:Number,
        default:0
    },
    Fil3Num:{
        type:Number,
        default:0
    },
    Fil1LBL:{
        type:String,
        default:""
    },
    Fil2LBL:{
        type:String,
        default:""
    },
    Fil3LBL:{
        type:String,
        default:""
    },
    Fil1Cmb:{
        type:String,
        default:""
    },
    Fil2Cmb:{
        type:String,
        default:""
    },
    Fil3Cmb:{
        type:String,
        default:""
    },

    ip4: {
        type: String, required: true
    },
    ip6: {
        type: String, required: true
    },
    mac_address: {
        type: String, required: true
    }



}, { timestamps: true })


// Create Model
const EmailRequestModel = mongoose.model('report_request', ReportRequestSchema)

export default EmailRequestModel
// module.exports = EmailRequestModel;
