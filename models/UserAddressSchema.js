// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 


import mongoose from "mongoose";

const userAddressSchema = new mongoose.Schema({
    account_name: { type: String, trim: true },
    off_no: { type: String, trim: true },
    off_add1: { type: String, trim: true },
    off_add2: { type: String, trim: true },
    pincode: { type: String, trim: true }

}, { timestamps: true })



// Create Model
const UseraddressModel = mongoose.model('client_addressses', userAddressSchema)

export default UseraddressModel