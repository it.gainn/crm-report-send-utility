// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

import mongoose from "mongoose";

// Defining Schema
// category schema
const MenuSchema = new mongoose.Schema({
    menu_name: { type: String, required: true, trim: true },
    sp_name: { type: String, trim: true },
    db_name: { type: String, trim: true },
    Url_name: { type: String, trim: true },
    Sequence_Number: { type: String, trim: true },



}, { timestamps: true })

// Create Model
const MenuModel = mongoose.model('menus', MenuSchema)

export default MenuModel