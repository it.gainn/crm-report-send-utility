// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

const mongoose =  require("mongoose");

// Defining Schema
// category schema
const TokenSchema = new mongoose.Schema({
    token: { type: String, required: true, trim: true },
   
    
}, { timestamps: true })

// Create Model
const TokenModel = mongoose.model('login_token', TokenSchema)

// export default TokenModel
module.exports = TokenModel