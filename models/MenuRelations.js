// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

import mongoose from "mongoose";

// category schema
const MenuRelations = new mongoose.Schema({
    role_id:{type: mongoose.Schema.ObjectId,trim:true},
    menu_id:[{type: mongoose.Schema.ObjectId,trim:true}],
    submenu_id:[{type: mongoose.Schema.ObjectId,trim:true}],
    subsubmenu_id:[{type: mongoose.Schema.ObjectId,trim:true}],    
    date: { type: Date, default: Date.now },

})


// Create Model
const MenuRelation = mongoose.model('menu_relations',MenuRelations)

export default MenuRelation
