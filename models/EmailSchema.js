// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

const mongoose = require("mongoose");

// Defining Schema
// category schema
const EmailSchema = new mongoose.Schema({
    email: { type: String, required: true, trim: true },
    password: { type: String, trim: true },
    
}, { timestamps: true })

// Create Model
const EmailModel = mongoose.model('email_credentials', EmailSchema)

// export default EmailModel
module.exports = EmailModel