// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

const  mongoose = require("mongoose")


// Defining Schema
const ReportRequestSchema = new mongoose.Schema({

    client_code: {
        type: String, trim: true
    },
    email_to: {
        type: String, trim: true
    },
    email_from: {
        type: String, trim: true
    },
    file_format: {
        type: String, trim: true
    },
    send_request_type: {
        type: String, trim: true
    },
    number: {
        type: String, trim: true
    },
    filename: {
        type: String, trim: true
    },
    document: {
        type: String, trim: true
    },
    telegram: {
        type: String, trim: true
    },
    caption: {
        type: String, trim: true
    },

    from_date: {
        type: String, trim: true,
    },
    to_date: {
        type: String, trim: true,
    },
    report_name: {
        type: String, trim: true,
    },
    status: {
        type: String, trim: true, default: '0'
    },
    mail_status: {
        type: String, trim: true, default: '0'
    },
    request_datetime: {
        type: String, required: true
    },
    
    created_by: {
        type: mongoose.Schema.ObjectId, trim: true, required: true
    },

    // dynemic cronjob
    
    ip4: {
        type: String, required: true
    },
    ip6: {
        type: String, required: true
    },
    mac_address: {
        type: String, required: true
    },
    file_create_status: {
        type: String, trim: true, default: '0'
    },
    file_create_datetime:{
        type: String, required: true
    },
    file_sendtomail_datetime:{
        type: String, required: true
    },
    report_file_path:{
        type: String, 
    },
    report_send_path:{
        type: String, 
    },
    



}, { timestamps: true })


// Create Model
const EmailRequestModel = mongoose.model('trade_report_request', ReportRequestSchema)

// export default EmailRequestModel

module.exports = EmailRequestModel;
