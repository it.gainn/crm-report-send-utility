// Author : 
// Date (dd-mmm-yy) : 
// Last updated by : 
// Last updated Date (dd-mmm-yy):  
// Description : 

const mongoose =  require("mongoose");

// Defining Schema
// category schema
const PancardSchema = new mongoose.Schema({
    pan_no: { type: String, trim: true },
   
    
}, { timestamps: true })

// Create Model
const PancardModel = mongoose.model('user_pancards', PancardSchema)

// export default PancardModel
module.exports = PancardModel